InforSanLuis-Flutter
Diseñando y Desarrollando aplicaciones con Flutter

** Colaboradores:
	
    Maguire, Margarita -- meganmaguire000@gmail.com
    Decena, Facundo M. -- facundo.decena@gmail.com
    Pellegrino, Maximiliano H. -- pellegrinomhd@gmail.com
    Merenda, Franco N. -- merenda.franco83@gmail.com

Este proyecto tiene dentro un ejemplo muy básico de como hacer una llamada a una API, obtener los datos en formato JSON, 
decodificarlo y mostrar en pantalla su información.

*Sugerencia: Modificarlo de tal manera de que quede una interfaz moderna y amigable con el usuario. La actual está solo realizada a fin de 
mostrar la información de una manera muy básica.

Link de Referencia: https://flutter.dev/docs/cookbook/networking/fetch-data


