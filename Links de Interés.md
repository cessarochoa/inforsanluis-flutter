 # InforSanLuis-Flutter

# Diseñando y Desarrollando aplicaciones con Flutter
![Image](https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F82532491%2F173310819539%2F1%2Foriginal.20191125-145242?h=2000&w=720&auto=compress&s=68911c537dd3338a9f222cb922e2e7d6)<br>
<br>
[ Link de Inscripción al Taller ](https://www.eventbrite.com.ar/e/desarrollando-y-disenando-aplicaciones-moviles-con-flutter-tickets-83487700961)<br>
## Contactos 
* Colaboradores: 
    * Maguire, Margarita -- meganmaguire000@gmail.com
    * Decena, Facundo M. -- facundo.decena@gmail.com
    * Pellegrino, Maximiliano H. -- pellegrinomhd@gmail.com
    * Merenda, Franco N. -- merenda.franco83@gmail.com

---

### Links de interés:

* [F.A.Q. Flutter - ¿ Por qué Dart ?](https://flutter.dev/docs/resources/faq#introduction)
* [Technical Overview](https://flutter.dev/docs/resources/technical-overview#why-use-flutter)
* [Tour de Dart](https://dart.dev/guides/language/language-tour)
* [Dart Cheatsheet](https://dart.dev/codelabs/dart-cheatsheet) 
* [Buenas prácticas en Dart](https://dart.dev/guides/language/effective-dart)
* [Getters y Setters](https://stackoverflow.com/questions/27683924/how-do-getters-and-setters-change-properties-in-dart)
* [Async/Await - Dart](https://dart.dev/codelabs/async-await)
* [Async/Await - Dart - Ayuda](https://codingwithjoe.com/dart-fundamentals-async-await/)
* [Stateless y Stateful Widgets - Flutter](https://flutter.dev/docs/development/ui/interactive#stateful-and-stateless-widgets)
* [Catálogo de Widgets](https://flutter.dev/docs/development/ui/widgets)
* [Indice de Widgets alfabéticamente](https://flutter.dev/docs/reference/widgets)
* [Widget of the Week - Playlist](https://www.youtube.com/playlist?list=PLjxrf2q8roU23XGwz3Km7sQZFTdB996iG)
* [Recursos de Material Design](https://material.io/resources/)
* [Color Tool- Para experimentar con los colores de una Material App](https://material.io/resources/color/)
* [Estudios de aplicaciones, para inspirarse](https://material.io/design/material-studies/)
* [Google Fonts](https://fonts.google.com/)
* [FontPair, ayuda a elegir un par de tipografías que combinen](https://fontpair.co/)
* [Pinterest, para buscar inspiración](https://www.pinterest.es/)

#### Links asociados a configuración e Instalación de Flutter
* [Android Studio](https://developer.android.com/studio#downloads)
* [Configurando aceleración de Emulador](https://developer.android.com/studio/run/emulator-acceleration#accel-vm)
* [Habilitando opciones de Desarrollador](https://developer.android.com/studio/debug/dev-options.html)
* [AndroidX Migration](https://flutter.dev/docs/development/androidx-migration)



#### Flutter Online
* [Probar Flutter Online](https://dartpad.dev/embed-flutter.html)

